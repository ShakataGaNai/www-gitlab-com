---
layout: handbook-page-toc
title: "How to Reach Us"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

When trying to contact the team consider the following. 

## How to reach the right member of the People Group

This table lists the aliases to use, when you are looking to reach a specific group in the People Group. It will also ensure you get the right attention, from the right team member, faster.

| Subgroup                          | GitLab handle   | Email            |  Slack | Greenhouse |
|-----------------------------------|-----------------|------------------|-----------------|-----------------|
| [People Business Partners](https://gitlab.com/gitlab-com/people-ops/General) | @gl-peoplepartners | peoplepartners@domain | n/a | n/a |
| [Compensation and benefits](https://gitlab.com/gitlab-com/people-ops/Compensation) | @gl-compensation | compensation@domain | n/a | n/a |    
| [People Operations General](https://gitlab.com/gitlab-com/people-ops/General) | @gl-peopleops  | peopleops@domain |n/a | n/a |
| [Diversity and Inclusion](https://gitlab.com/gitlab-com/diversity-and-inclusion) | No alias yet, @mention the [Diversity and Inclusion Partner](/job-families/people-ops/diversity-inclusion-partner/) | diversityinclusion@domain | n/a | n/a |
| [Learning and Development](https://gitlab.com/gitlab-com/people-ops/Training) | No alias yet, @mention the [Learning and Development Generalist](/job-families/people-ops/learning-development-specialist/) | learning@domain | n/a | n/a |
| [Employer Branding](https://gitlab.com/gitlab-com/people-ops/recruiting) | No alias yet, @ mention the [Employer Branding Lead](/job-families/people-ops/employment-branding-specialist/) | employmentbranding@domain | n/a | n/a |
| [Recruiting](https://gitlab.com/gitlab-com/people-ops/recruiting) | @gl-recruiting | recruiting@domain | @recruitingteam | n/a |
| [Candidate Experience Specialist](https://gitlab.com/gitlab-com/people-ops/recruiting) | @gl-ces | ces@domain |@ces | @ces* |
| Recruiting Operations| @gl-recruitingops | recruitingops@domain | @recruitingops | @recruitingops |

## People Business Partner Alignment to Groups

| Contact                         | Group  | 
|-----------------------------------|-----------------|
| Carolyn Bednarz | Sales - Enterprise, Commercial and Global Channels |
| Jessica Mitchell | Marketing, Product and Meltano |
| Julie Armendariz | Finance, Legal and People |
| Lorna Webster | Sales - Alliances, Customer Success and Field Operations |
| Roos Takken | Engineering