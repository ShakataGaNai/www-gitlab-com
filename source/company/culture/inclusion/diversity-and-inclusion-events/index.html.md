---
layout: markdown_page
title: "Diversity and Inclusion Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction







Summary of Events for 2019:

| Month    | Events                                                       | Length                    |
|----------|--------------------------------------------------------------|---------------------------|
| Apr 2019 | Hired D&I Manager                                            |
| May 2019 | Incorporated D&I Breakout Sessions at Contribute 2019        | Yearly
| Jun 2019 | Started Monthly D&I Initiatives Call                         | Monthly - Ongoing
|          | Inclusive Language Training                                  | 
|          | Created GitLab D&I Mission Statement
|          | Added GitLab's Defintion of Diversity & Inclusion
|          | Purchased Greenhouse Inclusion Tool
| Aug 2019 | All Call for D&I Advisory Group Applications                 |
|          | Opened Sign Ups for ERGs (Employee Resource Groups)          | 
|          | Kickoff Calls for ERGs
|          | GitLab Pride                                                 | Ongoing
|          | GitLab MIT - Minorities in Tech                              | Ongoing
|          | GitLab DiversABILITY                                         | Ongoing
|          | GitLab Women+                                                | Ongoing
|          | Kickoff Call for D&I Advisory Group
| Sep 2019 | Created D&I Advisory Group Guidelines
|          | Created ERG Guidelines
|          | Created D&I Framework      
| Oct 2019 | Added Slack Channels for all ERGs and D&I Advisory Group
| Dec 2019 | Live Learning Inclusion Training
|          | Received D&I Comparably Award


Summary of Events for 2020:

| Month    | Events                                                                                                                        |
|----------|-------------------------------------------------------------------------------------------------------------------------------|
| Jan 2020 | Anita Borg contract signed    
|          | Live Learning Ally Training                                                                                |
|          | D&I Analytics Dashboard - First Iteration
| Feb 2020 | Anita Borg becomes an Official Partner 
|          | D&I Survey via Culture Amp               |
| Mar 2020 | Unconscious Bias Training                                     |
|          | Working Mother Media Award Submission
|          | D&I Activities at Contribute                                                     |
| Apr      | Kickoff Women in Sales Initiatives                                     |
| Apr      | Kickoff D&I in Engineering Initiatives                                     |
